﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Http.Description;
using BlogEngine.Core;
using ViewModel;

namespace BLL
{
    public class BloggerRepository
    {
        public bool CheckBlogName(string blogName)
        {
            using (var db = new AVCommunityEntities())
            {
                return (db.be_Blogs.FirstOrDefault(b => b.BlogName == blogName) == null);
            }
        }
        public void Register(RegisterViewModel model)
        {
            var settings = new Dictionary<string, string>
            {
                {"displayratingsonrecentposts", "True"},
                {"sendmailoncomment", "True"},
                {"showpostnavigation", "True"},
                {"mobiletheme", "JQ-Mobile"},
                {"enablehttpcompression", "False"},
                {"enablerelatedposts", "True"},
                {"htmlheader", ""},
                {"theme", "AldrigVila"},
                {"emailsubjectprefix", "Weblog"},
                {"trackingscript", ""},
                {"showdescriptioninpostlist", "False"},
                {"commentsperpage", "10"},
                {"description", ""},
                {"postsperfeed", "10"},
                {"enablepingbacksend", "True"},
                {"selfregistrationinitialrole", "Select"},
                {"showdescriptioninpostlistforpostsbytagorcategory", "False"},
                {"commentblacklistcount", "2"},
                {"enabletrackbacksend", "True"},
                {"iscocommentenabled", "False"},
                {"compresswebresource", "False"},
                {"addiptowhitelistfilteronapproval", "False"},
                {"themecookiename", "theme"},
                {"showincludecommentsoption", "False"},
                {"trustauthenticatedusers", "True"},
                {"endorsement", "http://www.dotnetblogengine.net/syndication.axd"},
                {"contactformmessage", "<p>I'll answer the mail as soon as I can.</p>"},
                {"searchbuttontext", "Search"},
                {"enablecommentsearch", "True"},
                {"remotefiledownloadtimeout", "30000"},
                {"israzortheme", "False"},
                {"numberofrecentcomments", "5"},
                {"enablecontactattachments", "True"},
                {"blogrollmaxlength", "23"},
                {"displaycommentsonrecentposts", "True"},
                {"smtpserver", "mail.example.com"},
                {"disqusdevmode", "False"},
                {"postsperpage", "5"},
                {"enabletrackbackreceive", "True"},
                {"enableopensearch", "True"}, 
                {"dayscommentsareenabled", "0"}, 
                {"useblognameinpagetitles", "True"},
                {"removeextensionsfromurls", "True"},
                {"smtpusername", "username"},
                {"enablepasswordreset", "True"},
                {"disqusaddcommentstopages", "False"},
                {"alternatefeedurl", ""},
                {"email", model.EMail},
                {"feedauthor", ""},
                {"enablecommentsmoderation", "False"},
                {"enableerrorlogging", "False"},
                {"culture", "Auto"},
                {"disquswebsitename", ""},
                {"handlewwwsubdomain", ""},
                {"removewhitespaceinstylesheets", "True"},
                {"allowservertodownloadremotefiles", "True"},
                {"remotemaxfilesize", "524288"},
                {"redirecttoremovefileextension", "False"},
                {"syndicationformat", "Rss"},
                {"iscommentsenabled", "True"},
                {"blogrollvisibleposts", "3"},
                {"geocodinglatitude", "0"},
                {"numberofreferrerdays", "1"},
                {"thumbnailserviceapi", "http://www.robothumb.com/src/?url={0}"},
                {"commentwhitelistcount", "1"},
                {"searchdefaulttext", "Enter search term or APML url"},
                {"smtpserverport", "25"},
                {"smtppassword", "password"},
                {"blockauthoroncommentdelete", "False"},
                {"iscommentnestingenabled", "True"},
                {"timestamppostlinks", "False"},
                {"avatar", "wavatar"},
                {"blogrollupdateminutes", "60"},
                {"enableoptimization", "False"},
                {"enablereferrertracking", "False"},
                {"moderationtype", "Auto"},
                {"enablepingbackreceive", "True"},
                {"showlivepreview", "True"},
                {"enablequicknotes", "True"},
                {"galleryfeedurl", "http://dnbegallery.org/feed/FeedService.svc"},
                {"searchcommentlabeltext", "Include comments in search"},
                {"addiptoblacklistfilteronrejection", "False"},
                {"timezone", "0"},
                {"enableenclosures", "False"},
                {"authorname", string.Format("{0} {1}", model.FirstName, model.LastName)},
                {"name", model.BlogName},
                {"descriptioncharacters", "300"},
                {"enablessl", "False"},
                {"contactthankmessage", "<h1>Thank you</h1><p>The message was sent.</p>"},
                {"enablewebsiteincomments", "False"},
                {"descriptioncharactersforpostsbytagorcategory", "0"},
                {"commentreportmistakes", "False"},
                {"enablecountryincomments", "False"},
                {"numberofrecentposts", "10"},
                {"enablerecaptchaoncontactform", "False"},
                {"enableselfregistration", "False"},
                {"language", "sv-SE"},
                {"securityvalidationkey", "75159fc419124fbeb9507abf68f8a5037ef6234100cd48848f8330f9cb695b4879e5cd525235477aa199908ec1227785"},
                {"requiresslmetaweblogapi", "False"},
                {"enablerating", "True"},
                {"geocodinglongitude", "0"}
            };

            using (var db = new AVCommunityEntities())
            {
                var option = new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted,
                    Timeout = TimeSpan.FromMinutes(5)
                };

                using (var ts = new TransactionScope(TransactionScopeOption.Required, option))
                {
                    var blogId = Guid.NewGuid();

                    try
                    {
                        db.be_Blogs.Add(new be_Blogs()
                        {
                            BlogId = blogId,
                            BlogName = model.BlogName,
                            IsAnyTextBeforeHostnameAccepted = true,
                            StorageContainerName = model.BlogName,
                            VirtualPath = "~/" + model.BlogName,
                            IsPrimary = false,
                            IsActive = true,
                            IsSiteAggregation = false,
                            Boost = 0,
                            Created = DateTime.Now,
                            IncludeInTopLists = true,
                            Hostname = string.Empty
                        });

                        db.SaveChanges();

                        foreach (var setting in settings)
                        {
                            db.be_Settings.Add(new be_Settings()
                            {
                                BlogId = blogId,
                                SettingName = setting.Key,
                                SettingValue = setting.Value
                            });
                        }

                        db.SaveChanges();

                        db.be_Users.Add(new be_Users()
                        {
                            BlogID = blogId,
                            EmailAddress = model.EMail,
                            Password = Utils.HashPassword(model.Password),
                            UserName = model.EMail
                        });

                        db.SaveChanges();

                        var blogFolder = HttpContext.Current.Server.MapPath(string.Format("~/App_Data/blogs/{0}", model.BlogName));
                        var filesFolder = HttpContext.Current.Server.MapPath(string.Format("~/App_Data/blogs/{0}/files", model.BlogName));
                        var avatarFolder = HttpContext.Current.Server.MapPath(string.Format("~/App_Data/blogs/{0}/files/avatars", model.BlogName));

                        if (!Directory.Exists(blogFolder))
                        {
                            Directory.CreateDirectory(blogFolder);
                            Directory.CreateDirectory(filesFolder);
                            Directory.CreateDirectory(avatarFolder);
                        }

                        var configPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\web.config";
                        File.SetLastWriteTimeUtc(configPath, DateTime.UtcNow);

                        ts.Complete();
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                HttpContext.Current.Response.Write(string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                            }
                        }

                        HttpContext.Current.Response.End();
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
        }
        public PointsLevelViewModel GetLevelAndPoints(Guid blogId)
        {
            var result = new PointsLevelViewModel();

            using (var db = new AVCommunityEntities())
            {
                var blogDataKeyPoints = Helpers.GetKey(db, Constants.BlogPoints);
                var blogDataKeyLevel = Helpers.GetKey(db, Constants.BlogLevel);

                var blogDataPoints = Helpers.GetData(db, blogId, blogDataKeyPoints.Id, "0");
                var blogDataLevel = Helpers.GetData(db, blogId, blogDataKeyLevel.Id, "1");

                result.Points = blogDataPoints.DataValue;
                result.Level = blogDataLevel.DataValue;
            }

            return result;
        }

        public List<string> GetSocialNetworks(Guid blogId)
        {
            var result = new List<string>();

            using (var db = new AVCommunityEntities())
            {
                var blogDataKeySocialNetworks = Helpers.GetKey(db, Constants.BlogSocialNetworks);
                var blogDataSocialNetworks = Helpers.GetData(db, blogId, blogDataKeySocialNetworks.Id, "");

                var socialNetworks = blogDataSocialNetworks.DataValue.Split('|');

                result.AddRange(socialNetworks);
            }

            return result;
        }

        public List<string> GetSponsors(Guid blogId)
        {
            var result = new List<string>();

            using (var db = new AVCommunityEntities())
            {
                var blogDataKeySponsors = Helpers.GetKey(db, Constants.BlogSponsors);
                var blogDataSponsors = Helpers.GetData(db, blogId, blogDataKeySponsors.Id, "");

                var sponsors = db.be_Sponsors.ToList().Where(s => blogDataSponsors.DataValue.Split('|').Contains(s.Name));

                result.AddRange(sponsors.Select(sponsor => sponsor.Path));
            }

            return result;
        }
    }
}

