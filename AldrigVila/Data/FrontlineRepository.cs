﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for FrontlineRepository
/// </summary>
public class FrontlineRepository
{
    public IEnumerable<FrontLineViewModel> GetFrontLine()
    {
        using (var db = new AVCommunityEntities())
        {
            var frontLines = (from p in db.be_Posts
                              join lpl in db.be_LevelTopList on p.BlogID equals lpl.BlogId
                              join pf in db.be_Profiles on p.BlogID equals pf.BlogID
                              join b in db.be_Blogs on new { BlogID = p.BlogID } equals new { BlogID = b.BlogId }
                              where
                                pf.SettingName == "photourl" &&
                                pf.SettingValue != ""
                              group new { pf, b, p, lpl } by new
                              {
                                  pf.BlogID,
                                  pf.UserName,
                                  pf.SettingValue,
                                  b.BlogName,
                                  lpl.LevelAmount
                              } into g
                              //g.Sum(p => p.p.UniqueViews) descending
                              select new FrontLineViewModel
                              {
                                  UserName = g.Key.UserName,
                                  BlogName = g.Key.BlogName,
                                  FileName = g.Key.SettingValue,
                                  Level = g.Key.LevelAmount
                              }).ToList().OrderByDescending(x => x.Level).Take(14);

            return frontLines;
        }
    }
}