﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Helpers
/// </summary>
public static class Helpers
{
    public static be_BlogDataKeys GetKey(AVCommunityEntities db, string keyName)
    {
        var result = db.be_BlogDataKeys.FirstOrDefault(bdk => bdk.DataKey == keyName);

        if (result == null)
        {
            result = db.be_BlogDataKeys.Add(new be_BlogDataKeys()
            {
                Id = Guid.NewGuid(),
                DataKey = keyName
            });

            db.SaveChanges();
        }

        return result;
    }

    public static be_BlogData GetData(AVCommunityEntities db, Guid blogId, Guid dataKeyId, string defaultValue)
    {
        var result = db.be_BlogData.FirstOrDefault(bd => bd.BlogId == blogId && bd.DataKeyId == dataKeyId);

        if (result == null)
        {
            result = new be_BlogData()
            {
                BlogId = blogId,
                DataKeyId = dataKeyId,
                DataValue = defaultValue,
                Created = DateTime.Now,
                Updated = DateTime.Now
            };

            db.be_BlogData.Add(result);
            db.SaveChanges();
        }

        return result;
    }
}