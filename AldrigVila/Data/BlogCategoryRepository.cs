﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

public class BlogCategoryRepository
{
    public List<string> GetCategories()
    {
        using (var db = new AVCommunityEntities())
        {
            return db.be_Categories.Select(c => c.CategoryName).Distinct().ToList();
        }
    }

    private string GetImageSrc(string content)
    {
        const string pattern = "<img.+?src=[\"'](.+?)[\"'].*?>";
        var matches = Regex.Matches(content, pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        if (matches.Count > 0)
            return matches[0].Groups[1].Value;

        return string.Empty;
    }

    public List<ArchiveViewModel> GetArchive(string category)
    {
        var result = new List<ArchiveViewModel>();

        using (var db = new AVCommunityEntities())
        {
            if (string.IsNullOrEmpty(category))
            {
                var posts = (from p in db.be_Posts
                            where p.PostContent.Contains("<img") && 
                            p.IsDeleted == false && 
                            p.IsPublished == true
                            select new ArchiveViewModel
                            {
                                Title = p.Title,
                                Created = p.DateCreated.HasValue ? p.DateCreated.Value : DateTime.MinValue,
                                CommentAmount = db.be_PostComment.Count(pc => pc.PostID == p.PostID && pc.IsDeleted == false && pc.IsApproved == true && pc.IsSpam == false),
                                PostContent = p.PostContent
                            }).ToList();

                result.AddRange(posts);
            }
            else
            {
                var posts = (from p in db.be_Posts
                             join pc in db.be_PostCategory on p.PostID equals pc.PostID
                             join c in db.be_Categories on pc.CategoryID equals c.CategoryID
                             where  c.CategoryName == category &&
                                    p.PostContent.Contains("<img") &&
                                    p.IsDeleted == false &&
                                    p.IsPublished == true
                            select new ArchiveViewModel
                            {
                                Title = p.Title,
                                Created = p.DateCreated.HasValue ? p.DateCreated.Value : DateTime.MinValue,
                                CommentAmount = db.be_PostComment.Count(pc2 => pc2.PostID == p.PostID && pc2.IsDeleted == false && pc2.IsApproved == true && pc2.IsSpam == false),
                                PostContent = p.PostContent
                            }).ToList();

                result.AddRange(posts);
            }
        }

        foreach (var archive in result)
        {
            archive.Image = GetImageSrc(archive.PostContent);
        }

        return result;
    }

    public List<LevelTopListViewModel> GetLevelTopList(string category)
    {
        var result = new List<LevelTopListViewModel>();
        
        using (var db = new AVCommunityEntities())
        {
            if (category == null)
            {
                var levelTopList = (from ltl in db.be_LevelTopList
                                    join b in db.be_Blogs on ltl.BlogId equals b.BlogId
                                    join pf in db.be_Profiles on ltl.BlogId equals pf.BlogID
                                    where
                                        pf.SettingName == "photourl" &&
                                        pf.SettingValue != ""
                                    orderby ltl.LevelAmount descending
                                    select new
                                    {
                                        ltl.BlogId,
                                        ltl.LevelAmount,
                                        ltl.PointsAmount,
                                        b.BlogName
                                    }).Take(12);

                foreach (var levelTop in levelTopList)
                {
                    var user = db.be_Users.FirstOrDefault(u => u.BlogID == levelTop.BlogId);

                    result.Add(new LevelTopListViewModel()
                    {
                        Author = user != null ? user.UserName : string.Empty,
                        LevelAmount = levelTop.LevelAmount,
                        PointsAmount = levelTop.PointsAmount,
                        BlogName = levelTop.BlogName
                    });
                }
            }
            else
            {
                var levelTopList = (from ltl in db.be_LevelTopList
                                    join b in db.be_Blogs on ltl.BlogId equals b.BlogId
                                    join pf in db.be_Profiles on ltl.BlogId equals pf.BlogID
                                    where
                                        pf.SettingName == "photourl" &&
                                        pf.SettingValue != ""
                                    orderby ltl.LevelAmount descending
                                    select new
                                              {
                                                  ltl.BlogId,
                                                  ltl.LevelAmount,
                                                  ltl.PointsAmount,
                                                  b.BlogName,
                                                  ltl.Categories
                                              }).Take(12).ToList().Where(x => x.Categories.Contains(category)).ToList();

                foreach (var levelTop in levelTopList)
                {
                    var user = db.be_Users.FirstOrDefault(u => u.BlogID == levelTop.BlogId);

                    result.Add(new LevelTopListViewModel()
                    {
                        Author = user != null ? user.UserName : string.Empty,
                        LevelAmount = levelTop.LevelAmount,
                        PointsAmount = levelTop.PointsAmount,
                        BlogName = levelTop.BlogName
                    });
                }
            }
        }

        return result;
    }

    public List<LevelTopListViewModel> GetRookies(string category)
    {
        var result = new List<LevelTopListViewModel>();

        using (var db = new AVCommunityEntities())
        {
            var blogPointsDataKey = Helpers.GetKey(db, Constants.BlogPoints);
            var blogLevelDataKey = Helpers.GetKey(db, Constants.BlogLevel);

            if (category == null)
            {
                var levelTopList = (from b in db.be_Blogs
                                    join pf in db.be_Profiles on b.BlogId equals pf.BlogID
                                    join bd in db.be_BlogData on new { BlogId = b.BlogId, Key = blogPointsDataKey.Id } equals new { BlogId = bd.BlogId, Key = bd.DataKeyId } into bdPoints
                                    join bd in db.be_BlogData on new { BlogId = b.BlogId, Key = blogLevelDataKey.Id } equals new { BlogId = bd.BlogId, Key = bd.DataKeyId } into bdLevel
                                    where
                                        pf.SettingName == "photourl" &&
                                        pf.SettingValue != ""
                                    orderby b.Created descending
                                    select new
                                    {
                                        b.BlogId,
                                        LevelAmount = bdLevel.FirstOrDefault().DataValue,
                                        PointsAmount = bdPoints.FirstOrDefault().DataValue,
                                        b.BlogName
                                    }).Take(12);

                foreach (var levelTop in levelTopList)
                {
                    var user = db.be_Users.FirstOrDefault(u => u.BlogID == levelTop.BlogId);

                    result.Add(new LevelTopListViewModel()
                    {
                        Author = user != null ? user.UserName : string.Empty,
                        LevelAmount = Convert.ToInt32(Convert.ToDecimal(levelTop.LevelAmount)),
                        PointsAmount = Convert.ToInt32(Convert.ToDecimal(levelTop.PointsAmount)),
                        BlogName = levelTop.BlogName
                    });
                }
            }
            else
            {
                var levelTopList = (from b in db.be_Blogs
                                    join pf in db.be_Profiles on b.BlogId equals pf.BlogID
                                    join bd in db.be_BlogData on new { BlogId = b.BlogId, Key = blogPointsDataKey.Id } equals new { BlogId = bd.BlogId, Key = bd.DataKeyId } into bdPoints
                                    join bd in db.be_BlogData on new { BlogId = b.BlogId, Key = blogLevelDataKey.Id } equals new { BlogId = bd.BlogId, Key = bd.DataKeyId } into bdLevel
                                    join pc in db.be_PostCategory on b.BlogId equals pc.BlogID
                                    join c in db.be_Categories on pc.CategoryID equals c.CategoryID
                                    where
                                        pf.SettingName == "photourl" &&
                                        pf.SettingValue != "" &&
                                        c.CategoryName == category
                                    orderby b.Created descending
                                    select new
                                    {
                                        b.BlogId,
                                        LevelAmount = bdLevel.FirstOrDefault().DataValue,
                                        PointsAmount = bdPoints.FirstOrDefault().DataValue,
                                        b.BlogName
                                    }).Distinct().Take(12);

                foreach (var levelTop in levelTopList)
                {
                    var user = db.be_Users.FirstOrDefault(u => u.BlogID == levelTop.BlogId);

                    result.Add(new LevelTopListViewModel()
                    {
                        Author = user != null ? user.UserName : string.Empty,
                        LevelAmount = Convert.ToInt32(Convert.ToDecimal(levelTop.LevelAmount)),
                        PointsAmount = Convert.ToInt32(Convert.ToDecimal(levelTop.PointsAmount)),
                        BlogName = levelTop.BlogName
                    });
                }
            }
        }

        return result;
    }

    public List<LevelTopListViewModel> GetLatestUpdated(string category)
    {
        var result = new List<LevelTopListViewModel>();

        using (var db = new AVCommunityEntities())
        {
            var blogPointsDataKey = Helpers.GetKey(db, Constants.BlogPoints);
            var blogLevelDataKey = Helpers.GetKey(db, Constants.BlogLevel);

            if (category == null)
            {
                var levelTopList = (from b in db.be_Blogs
                                    join pf in db.be_Profiles on b.BlogId equals pf.BlogID
                                    join bd1 in db.be_BlogData on new { BlogId = b.BlogId, Key = blogPointsDataKey.Id } equals new { BlogId = bd1.BlogId, Key = bd1.DataKeyId } into bdPoints
                                    join bd2 in db.be_BlogData on new { BlogId = b.BlogId, Key = blogLevelDataKey.Id } equals new { BlogId = bd2.BlogId, Key = bd2.DataKeyId } into bdLevel
                                    join p in db.be_Posts on b.BlogId equals p.BlogID
                                    where
                                        p.IsDeleted == false &&
                                        p.IsPublished == true &&
                                        pf.SettingName == "photourl" &&
                                        pf.SettingValue != ""
                                    orderby b.Created descending
                                    select new
                                    {
                                        b.BlogId,
                                        LevelAmount = bdLevel.FirstOrDefault().DataValue,
                                        PointsAmount = bdPoints.FirstOrDefault().DataValue,
                                        b.BlogName
                                    }).Take(12).Distinct();

                foreach (var levelTop in levelTopList)
                {
                    var user = db.be_Users.FirstOrDefault(u => u.BlogID == levelTop.BlogId);

                    result.Add(new LevelTopListViewModel()
                    {
                        Author = user != null ? user.UserName : string.Empty,
                        LevelAmount = Convert.ToInt32(Convert.ToDecimal(levelTop.LevelAmount)),
                        PointsAmount = Convert.ToInt32(Convert.ToDecimal(levelTop.PointsAmount)),
                        BlogName = levelTop.BlogName
                    });
                }
            }
            else
            {
                var levelTopList = (from b in db.be_Blogs
                                    join pf in db.be_Profiles on b.BlogId equals pf.BlogID
                                    join bd in db.be_BlogData on new { BlogId = b.BlogId, Key = blogPointsDataKey.Id } equals new { BlogId = bd.BlogId, Key = bd.DataKeyId } into bdPoints
                                    join bd in db.be_BlogData on new { BlogId = b.BlogId, Key = blogLevelDataKey.Id } equals new { BlogId = bd.BlogId, Key = bd.DataKeyId } into bdLevel
                                    join p in db.be_Posts on b.BlogId equals p.BlogID
                                    join pc in db.be_PostCategory on p.PostID equals pc.PostID
                                    join c in db.be_Categories on pc.CategoryID equals c.CategoryID
                                    where
                                        p.IsDeleted == false &&
                                        p.IsPublished == true &&
                                        pf.SettingName == "photourl" &&
                                        pf.SettingValue != "" &&
                                        c.CategoryName == category
                                    orderby b.Created descending
                                    select new
                                    {
                                        b.BlogId,
                                        LevelAmount = bdLevel.FirstOrDefault().DataValue,
                                        PointsAmount = bdPoints.FirstOrDefault().DataValue,
                                        b.BlogName
                                    }).Distinct().Take(12);

                foreach (var levelTop in levelTopList)
                {
                    var user = db.be_Users.FirstOrDefault(u => u.BlogID == levelTop.BlogId);

                    result.Add(new LevelTopListViewModel()
                    {
                        Author = user != null ? user.UserName : string.Empty,
                        LevelAmount = Convert.ToInt32(Convert.ToDecimal(levelTop.LevelAmount)),
                        PointsAmount = Convert.ToInt32(Convert.ToDecimal(levelTop.PointsAmount)),
                        BlogName = levelTop.BlogName
                    });
                }
            }
        }

        return result;
    }
}