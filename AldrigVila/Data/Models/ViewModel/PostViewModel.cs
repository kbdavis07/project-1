﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PostViewModel
/// </summary>
public class PostViewModel
{
    public Guid PostId { get; set; }
    public string UserName { get; set; }
    public string Title { get; set; }
    public string BlogName { get; set; }
    public string FileName { get; set; }
    public string PostContent { get; set; }
    public DateTime? CreateDate { get; set; }
    public string PostLink { get; set; }
    public int CommentsCount { get; set; }
}