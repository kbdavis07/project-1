﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LevelTopListViewModel
/// </summary>
public class LevelTopListViewModel
{
    public int LevelAmount { get; set; }
    public int PointsAmount { get; set; }
    public string Author { get; set; }
    public string BlogName { get; set; }
}