﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MonthYearViewModel
/// </summary>
public class MonthYearViewModel
{
    public string MonthName { get; set; }
    public int Month { get; set; }
    public int Year { get; set; }
}