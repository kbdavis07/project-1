﻿using System;

public class ArchiveViewModel
{
    public string Image { get; set; }
    public string Title { get; set; }
    public DateTime Created { get; set; }
    public int CommentAmount { get; set; }
    public string PostContent { get; set; }
}