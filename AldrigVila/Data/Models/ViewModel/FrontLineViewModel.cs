﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FrontLineViewModel
/// </summary>
public class FrontLineViewModel
{
    public string UserName { get; set; }
    public string BlogName { get; set; }
    public string FileName { get; set; }
    public int Level { get; set; }
}