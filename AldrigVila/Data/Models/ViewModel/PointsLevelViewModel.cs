﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PointsLevelViewModel
/// </summary>
public class PointsLevelViewModel
{
    public string Points { get; set; }
    public string Level { get; set; }
}