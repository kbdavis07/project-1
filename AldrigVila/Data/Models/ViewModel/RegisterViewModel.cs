﻿namespace ViewModel
{
    public class RegisterViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string BlogName { get; set; }
        public string Password { get; set; }
    }
}