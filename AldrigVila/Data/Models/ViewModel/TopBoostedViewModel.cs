﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TopBoostedViewModel
/// </summary>
public class TopBoostedViewModel
{
    public int TotalBoost { get; set; }
    public int Posts { get; set; }
    public int Boost { get; set; }
    public string Author { get; set; }
    public string BlogName { get; set; }
    public string Level { get; set; }
}