﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CategoriesViewModel
{
    public List<LevelTopListViewModel> LevelTopList { get; set; }
    public List<string> Categories { get; set; }
}