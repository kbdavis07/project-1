﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for PostRepository
/// </summary>
public class BEPostRepository
{
    public BEPostRepository()
    {
    }

    public IEnumerable<PostViewModel> GetPostsByCategory(string categoryName, int rows)
    {
        var db = new AVCommunityEntities();
        var category = db.be_Categories.FirstOrDefault(c => c.CategoryName == categoryName);

        if (category == null)
            throw new ArgumentException(string.Format("Category {0} does not exist", categoryName));

        var posts = (from p in db.be_Posts
                     join pc in db.be_PostCategory
                     on p.PostID equals pc.PostID
                     join b in db.be_Blogs
                     on p.BlogID equals b.BlogId
                     where pc.CategoryID == category.CategoryID
                     orderby p.DateCreated ascending
                     select new PostViewModel
                     {
                         PostId = p.PostID,
                         BlogName = b.BlogName,
                         CreateDate = p.DateCreated,
                         PostContent = p.PostContent,
                         Title = p.Title,
                         UserName = p.Author,
                         PostLink = p.PostLink
                     }).Take(rows);

        return posts;
    }

    public IEnumerable<be_Posts> GetPopular(int rows)
    {
        var db = new AVCommunityEntities();
        var posts = db.be_Posts.Where(p => p.PostContent.Contains("img src")).OrderByDescending(p => p.Rating).Take(rows);

        return posts;
    }

    public IEnumerable<PostViewModel> GetBlogLatest(int rows, Guid blogId)
    {
        var db = new AVCommunityEntities();
        var posts = (from p in db.be_Posts
                     join b in db.be_Blogs
                     on p.BlogID equals b.BlogId
                     where
                       p.BlogID == blogId && p.IsDeleted == false && p.IsPublished == true
                         orderby
                       p.DateCreated descending
                     select new PostViewModel
                     {
                         BlogName=b.BlogName,
                         PostId=p.PostID,
                         Title = p.Title,
                         PostContent = p.PostContent,
                         PostLink = p.PostLink,
                         CreateDate = p.DateCreated,
                         CommentsCount = db.be_PostComment.Count(pc => pc.PostID == p.PostID)                        
                     }).Take(rows);

        return posts;
    }

    public IEnumerable<PostViewModel> GetLatest(int rows)
    {
        var db = new AVCommunityEntities();
        var posts = (from p in db.be_Posts
                     join pf in db.be_Profiles on p.BlogID equals pf.BlogID
                     join b in db.be_Blogs on new { BlogID = p.BlogID } equals new { BlogID = b.BlogId }
                     where
                       pf.SettingName == "photourl" &&
                       pf.SettingValue != ""
                     orderby
                       p.DateCreated descending
                     select new PostViewModel
                     {
                         UserName = pf.UserName,
                         BlogName = b.BlogName,
                         FileName = pf.SettingValue,
                         Title = p.Title
                     }).Take(rows);

        return posts;
    }

    public IEnumerable<PostViewModel> GetLatest(string categoryName, int rows)
    {
        var db = new AVCommunityEntities();
        var category = db.be_Categories.FirstOrDefault(c => c.CategoryName == categoryName);

        if (category == null)
            throw new ArgumentException(string.Format("Category {0} does not exist", categoryName));

        var posts = (from p in db.be_Posts
                     join pf in db.be_Profiles on p.BlogID equals pf.BlogID
                     join b in db.be_Blogs on new { BlogID = p.BlogID } equals new { BlogID = b.BlogId }
                     join pc in db.be_PostCategory on p.PostID equals pc.PostID
                     where
                       pf.SettingName == "photourl" &&
                       pf.SettingValue != "" &&
                       pc.CategoryID == category.CategoryID
                     orderby
                       p.DateCreated descending
                     select new PostViewModel
                     {
                         UserName = pf.UserName,
                         BlogName = b.BlogName,
                         FileName = pf.SettingValue,
                         Title = p.Title,
                         CreateDate = p.DateCreated,
                         PostLink = p.PostLink
                     }).Take(rows);

        return posts;
    }
}