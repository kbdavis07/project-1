﻿using System;
using System.Globalization;
using System.Linq;

/// <summary>
/// Summary description for BoostRepository
/// </summary>
public class BoostRepository
{
    public BoostRepository()
    {
    }

    public TopBoostedViewModel GetTopBoosted()
    {
        using (var db = new AVCommunityEntities())
        {
            var result = new TopBoostedViewModel();
            var topBoostedList = db.be_TopBoosted.OrderByDescending(tb => tb.TotalBoostForWeek);

            if (topBoostedList.Any())
            {
                var topBoosted = topBoostedList.First();

                result.Boost = topBoosted.TotalBoostForWeek;
                result.Posts = topBoosted.TotalPosts;
                result.TotalBoost = topBoosted.TotalBoost;

                var blog = db.be_Blogs.FirstOrDefault(b => b.BlogId == topBoosted.BlogId);
                var user = db.be_Users.FirstOrDefault(u => u.BlogID == topBoosted.BlogId);
                var blogLevelKey = Helpers.GetKey(db, Constants.BlogLevel);
                var blogLevelData = Helpers.GetData(db, topBoosted.BlogId, blogLevelKey.Id, "1");

                if (blog != null)
                    result.BlogName = blog.BlogName;

                if (user != null)
                    result.Author = user.UserName;

                result.Level = Convert.ToDecimal(blogLevelData.DataValue).ToString("N0");
            }

            return result;
        }
    }

    private static int GetWeek()
    {
        var cul = CultureInfo.GetCultureInfo("sv-SE");

        var weekNum = cul.Calendar.GetWeekOfYear(DateTime.Today, CalendarWeekRule.FirstDay, DayOfWeek.Monday);

        return weekNum;
    } 
}