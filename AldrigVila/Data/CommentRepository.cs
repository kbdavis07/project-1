﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;

public class CommentRepository
{
    public void AddComment(CommentViewModel model)
    {
        using (var db = new AVCommunityEntities())
        {
            db.be_PostComment.Add(new be_PostComment()
            {
                Author = model.Name,
                Email = model.Email,
                Comment = model.Comment,
                CommentDate = DateTime.Now
            });
        }
    }
}