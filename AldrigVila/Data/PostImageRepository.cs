﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PostImageRepository
/// </summary>
public class PostImageRepository
{
    public enum PostImageSortBy
    {
        Latest,
        Rating
    }

    public PostImageRepository()
    {
    }

    public IEnumerable<be_PostImages> GetGallery(PostImageSortBy postImageSortBy, int rows)
    {
        var db = new AVCommunityEntities();
        var images = Enumerable.Empty<be_PostImages>();

        switch (postImageSortBy)
        {
            case PostImageSortBy.Latest:
                images = db.be_PostImages.OrderByDescending(pi => pi.Id).Take(rows);
                break;
            case PostImageSortBy.Rating:
                images = db.be_PostImages.OrderByDescending(pi => pi.Rating).Take(rows);
                break;
        }

        return images;
    }
}