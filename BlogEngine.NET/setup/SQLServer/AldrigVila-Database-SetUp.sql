﻿
-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------


-- Creating table 'be_Blogs'
CREATE TABLE [dbo].[be_Blogs] (
    [BlogRowId] int IDENTITY(1,1) NOT NULL,
    [BlogId] uniqueidentifier  NOT NULL,
    [BlogName] nvarchar(255)  NOT NULL,
    [Hostname] nvarchar(255)  NOT NULL,
    [IsAnyTextBeforeHostnameAccepted] bit  NOT NULL,
    [StorageContainerName] nvarchar(255)  NOT NULL,
    [VirtualPath] nvarchar(255)  NOT NULL,
    [IsPrimary] bit  NOT NULL,
    [IsActive] bit  NOT NULL,
    [IsSiteAggregation] bit  NOT NULL,
    [Boost] int  NOT NULL,
    [Created] datetime  NOT NULL,
    [IncludeInTopLists] bit  NOT NULL
);
GO

-- Creating table 'be_BoostLog'
CREATE TABLE [dbo].[be_BoostLog] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BlogId] uniqueidentifier  NOT NULL,
    [ParentId] uniqueidentifier  NOT NULL,
    [TypeId] int  NOT NULL,
    [Amount] int  NOT NULL,
    [Week] int  NOT NULL,
    [Month] int  NOT NULL,
    [Year] int  NOT NULL
);
GO


-- Creating table 'be_PostImages'
CREATE TABLE [dbo].[be_PostImages] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PostID] uniqueidentifier  NOT NULL,
    [ImageURL] nvarchar(500)  NOT NULL,
    [Raters] int  NOT NULL,
    [Rating] int  NOT NULL
);
GO


-- Creating table 'be_Posts'
CREATE TABLE [dbo].[be_Posts] (
    [PostRowID] int IDENTITY(1,1) NOT NULL,
    [BlogID] uniqueidentifier  NOT NULL,
    [PostID] uniqueidentifier  NOT NULL,
    [Title] nvarchar(255)  NULL,
    [Description] nvarchar(max)  NULL,
    [PostContent] nvarchar(max)  NULL,
    [DateCreated] datetime  NULL,
    [DateModified] datetime  NULL,
    [Author] nvarchar(50)  NULL,
    [IsPublished] bit  NULL,
    [IsCommentEnabled] bit  NULL,
    [Raters] int  NULL,
    [Rating] real  NULL,
    [Boosters] int  NULL,
    [Boost] int  NULL,
    [Slug] nvarchar(255)  NULL,
    [IsDeleted] bit  NOT NULL,
    [Views] int  NULL,
    [UniqueViews] int  NULL,
    [PostLink] nvarchar(200)  NULL
);
GO


-- Creating table 'be_RateBoostTypes'
CREATE TABLE [dbo].[be_RateBoostTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'be_RateLog'
CREATE TABLE [dbo].[be_RateLog] (
    [Id] int  NOT NULL,
    [BlogId] uniqueidentifier  NOT NULL,
    [ParentId] uniqueidentifier  NOT NULL,
    [TypeId] int  NOT NULL,
    [Amount] int  NOT NULL,
    [Week] int  NOT NULL,
    [Month] int  NOT NULL,
    [Year] int  NOT NULL
);
GO
-- Creating table 'be_BlogData'
CREATE TABLE [dbo].[be_BlogData] (
    [BlogId] uniqueidentifier  NOT NULL,
    [DataKeyId] uniqueidentifier  NOT NULL,
    [DataValue] varchar(8000)  NULL,
    [Created] datetime  NULL,
    [Updated] datetime  NULL
);
GO

-- Creating table 'be_BlogDataKeys'
CREATE TABLE [dbo].[be_BlogDataKeys] (
    [Id] uniqueidentifier  NOT NULL,
    [DataKey] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'be_LevelTopList'
CREATE TABLE [dbo].[be_LevelTopList] (
    [BlogId] uniqueidentifier  NOT NULL,
    [LevelAmount] int  NOT NULL,
    [PointsAmount] int  NOT NULL,
    [Categories] nvarchar(100)  NOT NULL
);
GO

-- Creating table 'be_Sponsors'
CREATE TABLE [dbo].[be_Sponsors] (
    [Id] uniqueidentifier  NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Path] nvarchar(200)  NOT NULL
);
GO

-- Creating table 'be_TopBoosted'
CREATE TABLE [dbo].[be_TopBoosted] (
    [BlogId] uniqueidentifier  NOT NULL,
    [TotalBoost] int  NOT NULL,
    [TotalPosts] int  NOT NULL,
    [TotalBoostForWeek] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id], [BlogId], [ParentId], [TypeId] in table 'be_BoostLog'
ALTER TABLE [dbo].[be_BoostLog]
ADD CONSTRAINT [PK_be_BoostLog]
    PRIMARY KEY CLUSTERED ([Id], [BlogId], [ParentId], [TypeId] ASC);
GO


-- Creating primary key on [Id], [PostID] in table 'be_PostImages'
ALTER TABLE [dbo].[be_PostImages]
ADD CONSTRAINT [PK_be_PostImages]
    PRIMARY KEY CLUSTERED ([Id], [PostID] ASC);
GO


-- Creating primary key on [Id] in table 'be_RateBoostTypes'
ALTER TABLE [dbo].[be_RateBoostTypes]
ADD CONSTRAINT [PK_be_RateBoostTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id], [BlogId], [ParentId], [TypeId] in table 'be_RateLog'
ALTER TABLE [dbo].[be_RateLog]
ADD CONSTRAINT [PK_be_RateLog]
    PRIMARY KEY CLUSTERED ([Id], [BlogId], [ParentId], [TypeId] ASC);
GO


-- Creating primary key on [Id] in table 'be_BlogDataKeys'
ALTER TABLE [dbo].[be_BlogDataKeys]
ADD CONSTRAINT [PK_be_BlogDataKeys]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [BlogId], [Categories] in table 'be_LevelTopList'
ALTER TABLE [dbo].[be_LevelTopList]
ADD CONSTRAINT [PK_be_LevelTopList]
    PRIMARY KEY CLUSTERED ([BlogId], [Categories] ASC);
GO

-- Creating primary key on [Id] in table 'be_Sponsors'
ALTER TABLE [dbo].[be_Sponsors]
ADD CONSTRAINT [PK_be_Sponsors]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [BlogId] in table 'be_TopBoosted'
ALTER TABLE [dbo].[be_TopBoosted]
ADD CONSTRAINT [PK_be_TopBoosted]
    PRIMARY KEY CLUSTERED ([BlogId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
