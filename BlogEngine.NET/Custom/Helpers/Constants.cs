﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public static class Constants
{
    public static string BlogLevel = "BlogLevel";
    public static string BlogPoints = "BlogPoints";
    public static string BlogPointsLastRun = "BlogPointsLastRun";
    public static string BlogSocialNetworks = "BlogSocialNetworks";
    public static string BlogSponsors = "BlogSponsors";
}