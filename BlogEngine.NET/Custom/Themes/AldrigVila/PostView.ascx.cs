﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BlogEngine.Core;

public partial class themes_AldrigVila_PostView : BlogEngine.Core.Web.Controls.PostViewBase
{
    public bool ShowPostPager { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        CommentView1.Post = Post;
        var totaltAmountOfPosts = 0;
        var page = Request.QueryString["page"];

        using (var db = new AVCommunityEntities())
        {
            totaltAmountOfPosts = db.be_Posts.Count(p => p.BlogID == Blog.CurrentInstance.BlogId && p.IsDeleted == false && p.IsPublished == true);

            if (page != null)
            {
                if (Index == BlogSettings.Instance.PostsPerPage - 1 || Index == (totaltAmountOfPosts-((Convert.ToInt32(page)-1)*BlogSettings.Instance.PostsPerPage)) - 1)
                {
                    ShowPostPager = true;
                    pager1.Posts = Post.ApplicablePosts.ConvertAll(new Converter<Post, IPublishable>(delegate(Post p) { return p as IPublishable; }));
                }
            }
            else
            {
                if (Index == BlogSettings.Instance.PostsPerPage - 1)
                {
                    ShowPostPager = true;
                    pager1.Posts = Post.ApplicablePosts.ConvertAll(new Converter<Post, IPublishable>(delegate(Post p) { return p as IPublishable; }));
                }
            }
        }
    }
}