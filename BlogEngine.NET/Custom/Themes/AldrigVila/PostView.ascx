﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" CodeFile="PostView.ascx.cs" Inherits="themes_AldrigVila_PostView" %>
<%@ Register Src="~/User controls/BlogLatestPosts.ascx" TagPrefix="av" TagName="BlogLatestPosts" %>
<%@ Register Src="~/User controls/CommentView.ascx" TagName="CommentView" TagPrefix="uc" %>

<article class="post-a" id="post<%=Index %>">
	<header>
        <a href="<%=Post.RelativeOrAbsoluteLink %>"><h2><%=Server.HtmlEncode(Post.Title) %></h2></a>
        <p><%=Post.DateCreated.ToString("d MMMM, yyyy, HH:MM") %></p>
	</header>
	
    <p><%=Post.Content %></p>

<% 
    if (Location == BlogEngine.Core.ServingLocation.PostList)
    {
%>
        <a href="<%=Post.RelativeOrAbsoluteLink %>#comment"><h3>Kommentarer <span><%=Post.Comments.Count %></span></h3></a>
	
        <hr style="width: 100%; border-top: 1px solid; border-style: dashed; border-color: #3d3434; margin-top: 20px; margin-bottom: 20px"/>
<%  } 
%>

<% 
    if (Location == BlogEngine.Core.ServingLocation.SinglePost)
    {
%>
    <uc:CommentView ID="CommentView1" runat="server" />
    
<%  }

    if (ShowPostPager)
    {
%>
    <blog:PostPager ID="pager1" runat="server"></blog:PostPager>

    <%} %>
</article>
