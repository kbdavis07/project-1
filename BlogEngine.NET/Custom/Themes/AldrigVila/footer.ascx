﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="footer.ascx.cs" Inherits="themes_AldrigVila_footer" %>
 <footer id="footer" class="vcard" itemscope itemtype="http://schema.org/Organization">
                <nav>
                    <div>
                        <h3>Information <span class="fn org" itemprop="name">AldrigVila</span></h3>
                        <ul>
                            <li><a href="./">Om oss</a></li>
                            <li><a href="./">Användarvillkor</a></li>
                            <li><a href="./">Kontakt</a></li>
                        </ul>
                    </div>
                    <div>
                        <h3>Partners</h3>
                        <ul>
                            <li><a rel="external" href="./">AldrigVila</a></li>
                            <li><a rel="external" href="./">TheRestlessCast</a></li>
                            <li><a rel="external" href="./">Shop</a></li>
                        </ul>
                    </div>
                    <div>
                        <h3>Nätverk</h3>
                        <ul>
                            <li><a rel="external" href="./">Instagram</a></li>
                            <li><a rel="external" href="./">Youtube</a></li>
                            <li><a rel="external" href="./">Facebook</a></li>
                        </ul>
                    </div>
                </nav>
                <p><span>Copyright &copy; <span class="date">2014</span> AldrigVila</span> <span>Alla rättigheter reserverade</span> <span class="adr" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span class="street-address" itemprop="streetAddress">Bresjögatan 19</span>, <span class="postal-code" itemprop="postalCode">256 94</span>, <span class="locality" itemprop="addressLocality">Stockholm</span></span></p>
            </footer>