﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BlogEngine.Core;
public partial class themes_AldrigVila_site : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Security.IsAuthenticated)
        {
            
            anonymousUserPanel.Visible = false;
            loggedInUserPanel.Visible = true;
            lnkLoggedInUsername.Text = Security.CurrentUser.Identity.Name;
             
        }
        else
        {
            anonymousUserPanel.Visible =true;
            loggedInUserPanel.Visible = false;
        }
    }
}
