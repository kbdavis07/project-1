﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" Inherits="BlogEngine.Core.Web.Controls.CommentFormBase" %>
<%@ Import Namespace="BlogEngine.Core" %>
<div class="comment-form">
    <fieldset>
        <p>
            <span>
                <label for="cma">Namn</label>
                <input type="text" name="txtName" id="txtName" required />
            </span>
            <span>
                <label for="cmb">E-post</label>
                <input type="text" id="txtEmail" required />
            </span>
        </p>
        <p>
            <label for="cmc">Skriv din kommentar här..</label>
            <textarea id="txtContent" name="txtContent" required></textarea>
        </p>
        <p>
            <label for="cbNotify">Meddela mig när det kommer nya kommentarer</label>
            <input type="checkbox" id="cbNotify" style="border: 0px !important" />
        </p>
        <p>
            <button type="button" id="btnSaveAjax" onclick="return BlogEngine.validateAndSubmitCommentForm()">Lämna Kommentar   </button>
        </p>
    </fieldset>
</div>
