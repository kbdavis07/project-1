﻿<%@ Control Language="C#" EnableViewState="False" Inherits="BlogEngine.Core.Web.Controls.CommentViewBase" %>

<li>
	<header>
		<figure><%= Gravatar(72)%> </figure>
        <h4><%= Comment.Website != null ? "<a href=\"" + Comment.Website + "\" rel=\"nofollow\" class=\"url fn\">" + Comment.Author + "</a>" : "<span class=\"fn\">" +Comment.Author + "</span>" %></h4>
        <p><%= Comment.DateCreated  %> </p>
	</header>
			
    <p><%= Text %></p>

	<footer>
		<ul>
            <li><%=ReplyToLink %></li>
            <%=  AdminLinks %>
        </ul>
	</footer>

    <div id="replies_<%=Comment.Id %>" <%= (Comment.Comments.Count == 0 || Comment.Email == "pingback" || Comment.Email == "trackback") ? " style=\"display:none;\"" : "" %>>
        <ul style="margin-top:10px; list-style-type:none">
            <asp:PlaceHolder ID="phSubComments" runat="server" />
        </ul>
    </div> 
</li>
	
